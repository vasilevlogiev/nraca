﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DevOceanTask.Tests
{
    [TestClass]
    public class Tax_Should
    {
        [TestMethod]
        [DataRow("family", 2300, 2307, 2344, 2715)]
        [DataRow("family", 2300, 2600, 2344, 0)]
        [DataRow("cargo", 2332, 2369, 344789, 326768)]
        public void CalculatesTaxCorrect(string vehicleType, int boughtYear, int taxYear, int lightMiles, int expectedTax)
        {
            //Arrange
            Tax sut = new Tax(vehicleType, boughtYear, taxYear, lightMiles);

            //Act
            int result = sut.DueTax;

            //Assert
            Assert.AreEqual(expectedTax, result);
        }

        [TestMethod]
        [DataRow("family", 2300, 2307, 2344, 2715)]
        [DataRow("cargo", 2332, 2369, 344789, 326768)]
        public void ReturnsCorrectString(string vehicleType, int boughtYear, int taxYear, int lightMiles, int totalTax)
        {
            //Arrange
            int initialTax = vehicleType == "cargo" ? 
                Constants.Vehicle.CARGO_INITIAL_TAX : 
                Constants.Vehicle.FAMILY_INITIAL_TAX;
            int lightMilesTax = vehicleType == "cargo" ?
                (lightMiles / 1000) * Constants.Vehicle.CARGO_TAX_PER_LIGHT_MILE :
                (lightMiles / 1000) * Constants.Vehicle.FAMILY_TAX_PER_LIGHT_MILE;
            int discount = vehicleType == "cargo" ?
                (taxYear - boughtYear) * Constants.Vehicle.CARGO_TAX_DISCOUNT_PER_YEAR :
                (taxYear - boughtYear) * Constants.Vehicle.FAMILY_TAX_DISCOUNT_PER_YEAR;

            string expected = string.Format(Constants.OUTPUT_MESSAGE, initialTax, lightMilesTax, discount, totalTax);
            Tax sut = new Tax(vehicleType, boughtYear, taxYear, lightMiles);

            //Act
            string result = sut.ToString();

            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}
