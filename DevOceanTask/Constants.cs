﻿using System;

namespace DevOceanTask
{
    public static class Constants
    {
        public static class Vehicle
        {
            public const int CARGO_INITIAL_TAX = 10000;
            public const int FAMILY_INITIAL_TAX = 5000;

            public const int CARGO_TAX_PER_LIGHT_MILE = 1000;
            public const int FAMILY_TAX_PER_LIGHT_MILE = 100;

            public const int CARGO_TAX_DISCOUNT_PER_YEAR = 736;
            public const int FAMILY_TAX_DISCOUNT_PER_YEAR = 355;
        }

        public const string WELCOME_MESSAGE = "Welcome to NRACA - National Revenue Agency Console Application!\n\nEnter your vehicle type (cargo/family), bought year, current tax year, light miles, separated by space.\nExample: ";
        public const string INPUT_EXAMPLE = "cargo 2300 2350 111222";
        public const string OUTPUT_MESSAGE = "Initial tax: {0} DVS\nLight miles tax: {1} DVS\nDiscount: {2} DVS\nYou owe {3} DVS to the BIG SISTER!";
    }
}
