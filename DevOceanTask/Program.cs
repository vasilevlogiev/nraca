﻿using System;

namespace DevOceanTask
{
    public class Program
    {
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        public static void Main()
        {
            PrintInitialMessage();

            string input = Console.ReadLine();

            while (input != "end")
            {
                Tax tax = GetTax(input);
                PrintTax(tax);
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Prints the tax.
        /// </summary>
        /// <param name="tax">The tax.</param>
        private static void PrintTax(Tax tax)
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(tax);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
        }

        /// <summary>
        /// By given input string, splits it and creates new Tax instance.
        /// </summary>
        /// <returns>Tax instance</returns>
        private static Tax GetTax(string input)
        {
            string[] parameters = input.Split(' ');

            string vehicleType = parameters[0];
            int boughtYear = int.Parse(parameters[1]);
            int taxYear = int.Parse(parameters[2]);
            int lightMiles = int.Parse(parameters[3]);

            Tax tax = new Tax(vehicleType, boughtYear, taxYear, lightMiles);

            return tax;
        }

        /// <summary>
        /// Prints the initial message.
        /// </summary>
        private static void PrintInitialMessage()
        {
            Console.Write(Constants.WELCOME_MESSAGE);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Constants.INPUT_EXAMPLE);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
        }
    }
}
