﻿using System;

namespace DevOceanTask
{
    /// <summary>
    /// Tax model
    /// </summary>
    public class Tax
    {
        /// <summary>
        /// The initial tax field
        /// </summary>
        private readonly int initialTax;
        /// <summary>
        /// The light miles tax field
        /// </summary>
        private readonly int lightMilesTax;
        /// <summary>
        /// The discount field
        /// </summary>
        private readonly int discount;
        /// <summary>
        /// The vehicle type field
        /// </summary>
        private readonly string vehicleType;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tax"/> class.
        /// </summary>
        /// <param name="vehicleType">Type of the vehicle.</param>
        /// <param name="boughtYear">The bought year of the vehicle.</param>
        /// <param name="taxYear">The tax year.</param>
        /// <param name="lightMiles">The light miles of the vehicle.</param>
        /// <exception cref="System.ArgumentException">Bought year must be after current tax year!</exception>
        public Tax(string vehicleType, int boughtYear, int taxYear, int lightMiles)
        {
            if (boughtYear > taxYear)
                throw new ArgumentException("Bought year must be before current tax year!");

            this.vehicleType = vehicleType;

            this.initialTax = vehicleType == "cargo" ?
                Constants.Vehicle.CARGO_INITIAL_TAX :
                Constants.Vehicle.FAMILY_INITIAL_TAX;

            this.lightMilesTax = vehicleType == "cargo" ? 
                (lightMiles / 1000) * Constants.Vehicle.CARGO_TAX_PER_LIGHT_MILE : 
                (lightMiles / 1000) * Constants.Vehicle.FAMILY_TAX_PER_LIGHT_MILE;

            this.discount = vehicleType == "cargo" ?
                (taxYear - boughtYear) * Constants.Vehicle.CARGO_TAX_DISCOUNT_PER_YEAR :
                (taxYear - boughtYear) * Constants.Vehicle.FAMILY_TAX_DISCOUNT_PER_YEAR;
        }

        /// <summary>
        /// Gets the due tax.
        /// </summary>
        public int DueTax 
        {
            // Validates that discount is not bigger than sum of initial tax and tax for light miles. If is returns 0.
            // Just а comment - bad tax policy to decrease taxes for older cars. Maybe the Director of NCARPA is corrupted... But this is not my bussines, client wants this => receives this.
            get => (initialTax + lightMilesTax - discount) > 0 ?
                initialTax + lightMilesTax - discount :
                0;
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(Constants.OUTPUT_MESSAGE, this.initialTax, this.lightMilesTax, this.discount, this.DueTax);
        }
    }
}
