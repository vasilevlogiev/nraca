 # NRACA - National Revenue Agency Console Application

##### Author: [Vasil Evlogiev](https://gitlab.com/vasilevlogiev)

NRACA is a console application that helps its users understand what tax they owe the BIG SISTER for their vehicle.

The vehicles are two types:
- Cargo
- Family

When user enter data for vehicle in format [vehicleType boughtYear taxYear lightMiles] the app returns information about all taxes, discount and total tax. To stop the application just enter [end].

Also in this repository has test project with unit tests.

## Enjoy using NRACA
